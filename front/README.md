# Nuxt 3 Minimal Starter

We recommend to look at the [documentation](https://v3.nuxtjs.org).

## Setup

Make sure to install the dependencies

```bash
yarn install
```

### if you are not installing yarn

```
npm install -g yarn
```

confirm version
```
yarn -v
```

```
yarn init
```

### if you are not using this Node version
`^14.16.0 || ^16.11.0 || ^17.0.0"`

this run command
```
nodebrew install-binary latest  
```

confirm Node version
```
nodebrew -ls
```

use Node version
```
nodebrew use --LATEST_NODE_VERSION
```

go install command

```
yarn install
```

## Development

Start the development server on http://localhost:3000

```bash
yarn dev
```

## Production

Build the application for production:

```bash
yarn build
```

Checkout the [deployment documentation](https://v3.nuxtjs.org/docs/deployment).